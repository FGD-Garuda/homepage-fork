<img alt="GarudaLinux" src="./834_2.png" />

# Garuda Linux
This is source code of Garuda Linux website found at [https://garudalinux.org/](https://garudalinux.org/).


## Table of Contents
- [How to contribute?](#how-to-contribute)
- [Report a bug](#report-a-bug)
- [Repo Maintainers](#maintainers)
- [Contact](#contact)

## How to Contribute?
You can browse through existing issues or open a new issue  and submit a PR to fix that issue.
For detailed info read our [contribution guidelines](./CONTRIBUTING.md)

## Report a bug
If you encountered some problem on main website, check if issue has already been reported, if not, check [Reporting Bugs](https://wiki.garudalinux.org/en/reporting-bugs)

## Maintainers

- @librewish
- @SGSm
- @dal.delmonico
- @dr460nf1r3
- @Yorper
- @Namanlp
- @RohitSingh107
- @JustTNE
- @jonathon
- @PedroHLC
- @petsam
- @sumantv26
- @dalto.8
- @zoeruda 
- @Edu4rdSHL

## Contact
Contact us on [Forums](https://forum.garudalinux.org/).

## Social media

Follow us on  [Twitter](https://twitter.com/garudalinux) and [Facebook](https://www.facebook.com/garudalinux.org).
